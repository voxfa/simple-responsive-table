function simpleResponsiveTable(table){
	if($('.'+table).length){
		var counter = 1;
		var headers = [];
		var mobileTable = '<table class="sr-table mobile-table">';
		$('.'+table+' tr').each(function(){
			if (counter == 1){//Header
				$(this).children('td').each(function(){
					headers.push($(this).html());
				});
			} else {
				mobileTable += '<table class="mobile-row">';
				var i = 0;
				$(this).children('td').each(function(){
					mobileTable += '<tr><td class="mobile-table-header">'+headers[i]+'</td>';
					mobileTable += '<td>'+$(this).html()+'</td></tr>';
					i++;
				});
				mobileTable += '</table>';
			}
			counter++;
		});
		mobileTable += '</table>';
		$('.'+table).after(mobileTable);
	}
}